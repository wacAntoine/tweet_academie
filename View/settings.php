<!DOCTYPE html>
<html>
<head>
  <?php include 'View/head.php'; ?>
  <link rel="stylesheet" href="View/style2.css">
</head>
<body>
  <header>
    <?php include 'View/header.php'; ?>
  </header>
  <?php
  if(isset($login_error)) {
    foreach ($login_error as $key => $value) {
      echo '<div class="error_div">';
      echo $value;
      echo '</div>';
    }
  }
  ?>

  <div class="container">
    <h1>Settings</h1>
    <form>
    <div class="row">
    <div class="input-group">
      <span class="input-group-addon" id="basic-addon1">@</span>
      <input id="name" type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
    </div>
    <button id="searchUsername" type="button" class="btn btn-primary" id="send_username_change">Change Username</button>
    </div>
    <br>
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="input_email_change" aria-describedby="emailHelp" placeholder="Enter email">
        <button type="button" class="btn btn-primary" id="send_email_change">Change Email</button>
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Password</label>
        <input type="email" class="form-control" id="newPassword" aria-describedby="emailHelp" placeholder="New Password">
        <input type="email" class="form-control" id="newPasswordVerif" aria-describedby="emailHelp" placeholder="New Password Verification">
        <input type="email" class="form-control" id="lastPassword" aria-describedby="emailHelp" placeholder="Last Password">
        <button type="button" class="btn btn-primary" id="send_email_change">Change Password</button>
      </div>
    </form>
  </div>
<footer>
  <center>Twitter Copyright &copy; - Tous droits réservés.</center>
</footer>
</body>
</html>

<?php
class loggedController {

  public function disconnect() {
    session_destroy();
    include 'View/login.php';
  }

  public function settings() {
    include 'View/settings.php';
  }

  public function logged() {
    include 'View/logged.php';
  }
  
  public function search() {
    include 'View/search.php';
  }
}

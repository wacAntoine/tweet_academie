<?php
class indexController {
  public function login(){
    include 'Model/formsModel.php';
    $model = new formsModel;
    $valid = 0;
    $login_error = [];
    if (!$model->check_login_filds()) {
      array_push($login_error, 'Error : invalid form');
      include 'View/login.php';
      return 0;
    }
    if (!$model->check_login($_POST['email'], $_POST['pass'])) {
      array_push($login_error, 'Error : wrong email or password');
      $valid++;
    }

    if ($valid == 0){
      include 'Model/userModel.php';
      $user = new userModel;
      $user->my_session_start($_POST['email']);
      include 'View/logged.php';
    }
    else {
      include 'View/login.php';
    }
  }

  public function signin(){
    include 'Model/formsModel.php';
    $model = new formsModel;

    $signin_error = [];
    $valid = 0;

   if (!$model->check_signin_filds()) {
      array_push($signin_error, 'Error : invalid form');
      include 'View/login.php';
      return 0;
    }

    if (!$model->ckeck_singnin_filds_empty($_POST['username'],
        $_POST['email'], $_POST['pass'], $_POST['pass_verif'])) {
      array_push($signin_error, 'Error : the fields are empty');
      include 'View/login.php';
      return 0;
    }

    if (!$model->check_username($_POST['username'])) {
      array_push($signin_error, 'Error : invalid username');
      $valid++;
    }

    if (!$model->check_password($_POST['pass'], $_POST['pass_verif'])) {
      array_push($signin_error, 'Error : invalid password');
      $valid++;
    }

    if (!$model->check_email($_POST['email'])) {
      array_push($signin_error, 'Error : invalid email');
      $valid++;
    }

    if(!$model->check_email_exist($_POST['email'])) {
        array_push($signin_error, 'Error : that email already exists');
        $valid++;
    }

    if ($valid == 0) {
      $ready_to_login = 1;
      include 'View/login.php';
      $model->send_sign_in();
      return 0;

    }
    include 'View/login.php';
  }

}

<?php
class searchModel {
	public function search_username_model($search) {
		include_once 'database.php';
    	$dbh = database::connect();

		$search = "%" . $search . "%";
		$requette = "
		SELECT username, user_id
        FROM accounts
        Where username 
        LIKE :name
        ;";
        $requettePrepare = $dbh->prepare($requette);
    	$requettePrepare->bindParam(':name', $search, PDO::PARAM_STR);
        $requettePrepare->execute();
    	$users = $requettePrepare->fetchAll(PDO::FETCH_ASSOC);
		return $users;
	}
}
<!DOCTYPE html>
<html>
<head>
	<?php include 'View/head.php'; ?>
	<link rel="stylesheet" href="View/style3.css">
</head>
<body>
	<header>
		<?php include 'View/header.php'; ?>
		<!-- <h1>Twitter</h1> -->
	</header>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div id="imaginary_container">
					<div class="input-group stylish-input-group">
						<input id="searchInput" type="text" class="form-control"  placeholder="Search" >
						<span class="input-group-addon">
							<button id="searchButton" type="submit">
								<span>Search</span>
							</button>
						</span>
					</div>
						<div id='slotUser'></div>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<center>Twitter Copyright &copy; - Tous droits réservés.</center>
	</footer>
</body>
</html>

<?php
class tweetModel {
  public function send_tweet($tweet) {
    include_once 'database.php';
    $db = database::connect();
    $prepared = $db->prepare("
    INSERT INTO `tweets` (`tweet_id`, `user_id`, `author`, `content`, `tweeted_at`, `answer_to`)
    VALUES (NULL, :user_id, :user_id, :tweet, CURRENT_TIMESTAMP, '0')
    ;");
    $prepared->bindParam(':tweet', $tweet, PDO::PARAM_STR);
    $prepared->bindParam(':user_id', $_SESSION['user_id'], PDO::PARAM_STR);
    $prepared->execute();
  }

  public function get_tweets_model() {
    include_once 'database.php';
    $db = database::connect();
    $prepared = $db->prepare("
    SELECT *
    FROM tweets
    INNER JOIN accounts
    ON tweets.user_id = accounts.user_id
    WHERE tweets.user_id IN
    	(SELECT followed_id
    	FROM followers
    	WHERE user_id
    	LIKE :session_id)
    OR tweets.user_id = :session_id
    ORDER BY tweeted_at DESC
    LIMIT 30
    ;");
    $prepared->bindParam(':session_id', $_SESSION['user_id'], PDO::PARAM_STR);
    $prepared->execute();
    $obj = $prepared->fetchAll(PDO::FETCH_ASSOC);
    return $obj;
  }

  public function retweet_model($tweet_id, $user_id) {
    include_once 'database.php';
    $db = database::connect();
    $prepared = $db->prepare("
    SELECT * FROM tweets WHERE tweet_id LIKE :tweet_id
    ;");
    $prepared->bindValue(':tweet_id', $tweet_id, PDO::PARAM_STR);
    $prepared->execute();
    $obj = $prepared->fetchAll(PDO::FETCH_ASSOC);

    $prepared = $db->prepare("
    INSERT INTO `tweets` (`tweet_id`, `user_id`, `author`, `content`, `tweeted_at`, `answer_to`)
    VALUES (NULL, :user_id, :author, :content, CURRENT_TIMESTAMP, '0')
    ;");
    $prepared->bindValue(':user_id', $user_id, PDO::PARAM_STR);
    $prepared->bindValue(':content', $obj[0]['content'], PDO::PARAM_STR);
    $prepared->bindValue(':author', $obj[0]['author'], PDO::PARAM_STR);
    $prepared->execute();

    return $obj;
  }
}

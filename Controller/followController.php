<?php
class followController {
  public function send_follow_controller() {
    include "Model/followModel.php";
    $follower = $_SESSION['user_id'];
    $followed = $_POST['followed_id'];
    $model = new followModel;
    $model->send_follow_model($follower, $followed);
  }
  public function followers_view() {
    include 'View/followers.php';
  }
  public function followed_view() {
    include 'View/followed.php';
  }
}

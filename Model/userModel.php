<?php
class userModel {
  public function my_session_start($email) {

    //
    //

    // include 'database.php';
    $db = database::connect();

    $pre = $db->prepare('
    SELECT *
    FROM accounts
    WHERE email
    LIKE :email
    ;');
    $pre->bindParam(':email', $email, PDO::PARAM_STR);

    $pre->execute();
    $obj = $pre->fetchAll(PDO::FETCH_ASSOC);
    if (isset($obj) && isset($obj[0])) {
      $_SESSION['username'] = $obj[0]['username'];
      $_SESSION['email'] = $obj[0]['email'];
      $_SESSION['user_id'] = $obj[0]['user_id'];
    }
  }
}

// session_start();
// $var = new userModel;
// $var->my_session_start('asdf@asdf.asdf');
// echo "\n\nSESSION = " . $_SESSION['username'];
// echo "\n\nSESSION = " . $_SESSION['email'];
// echo "\n\nSESSION = " . $_SESSION['user_id'];

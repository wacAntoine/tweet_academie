<!DOCTYPE html>
<html>
<head>
  <?php include 'View/head.php'; ?>
  <link rel="stylesheet" href="View/style.css">
</head>
<body>
  <header>
    <h1>Tweet Academie</h1>
  </header>
  <?php
  if(isset($login_error)) {
    foreach ($login_error as $key => $value) {
      echo '<div class="error_div">';
      echo $value;
      echo '</div>';
    }
  }

  if(isset($ready_to_login) && $ready_to_login == 1) {
    echo '<div class="error_div green_div">';
    echo "Success : you can now log in";
    echo '</div>';
  }
   ?>
  <form action="index.php?controller=index&method=login" method="post">
    <fieldset>
      <legend>login</legend>
      <label for="nom">Email <em>:</em></label>
      <input name='email' placeholder="Email"><br>

      <label for="email">Password <em>:</em></label>
      <input placeholder="Password" name='pass' type='Password'><br>
      <input type='submit' class="btn btn-primary" value='Log in'>
    </fieldset>
  </form>
    <?php
    if(isset($signin_error)) {
      foreach ($signin_error as $key => $value) {
        echo '<div class="error_div">';
        echo $value;
        echo '</div>';
      }
    }
     ?>
     <form action="index.php?controller=index&method=signin" method="post">
    <fieldset>
      <legend>Signin</legend>
        <label for="nom">Username <em>:</em></label>
      <input  placeholder="Username" name='username'
          <?php
            if(isset($_POST['username']) && $_POST['username'] != '') {
              echo 'value="' . $_POST['username'] . '"';
            }
          ?>
      >
          <br>
        <label for="email">Email <em>:</em></label>
      <input placeholder='Email' type="email" name='email'
      <?php
        if(isset($_POST['email']) && $_POST['email'] != '') {
          echo 'value="' . $_POST['email'] . '"';
        }
      ?>
      >
        <br>
        <label for="email">Url Name <em>:</em></label>
      <input placeholder='Tag' type="tag" name='tag'
       <?php
        if(isset($_POST['tag']) && $_POST['tag'] != '') {
          echo 'value="' . $_POST['tag'] . '"';
        }
      ?>
      >
      <br>
        <label for="nom">Password <em>:</em></label>
      <input placeholder="Password" type='password' name='pass'><br>
        <label for="email">Password<em>:</em></label>
      <input placeholder="Password Confirmation" type='password' name='pass_verif'><br>
      <input type='submit' class="btn btn-primary" value='Sign in'>
    </fieldset>
  </form>

  <footer>
    <center>Twitter Copyright &copy; - Tous droits réservés.</center>
  </footer>
</body>
</html>

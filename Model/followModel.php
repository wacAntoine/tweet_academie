<?php
class followModel {
  public function send_follow_model($follower, $followed) {
	    include_once 'database.php';
	    $db = database::connect();
	    $prepared = $db->prepare("
	    INSERT INTO `followers` (`user_id`, `followed_id`, `followed_at`)
	    VALUES (:user_id, :followed_id, CURRENT_TIMESTAMP)
	    ;");
	    $prepared->bindValue(':user_id', $follower, PDO::PARAM_STR);
	    $prepared->bindValue(':followed_id', $followed, PDO::PARAM_STR);
	    return $prepared->execute();
	}
}

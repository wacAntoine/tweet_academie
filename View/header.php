<header class="text-center">
  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Tweet Academie</a>
      </div>
      <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="index.php?controller=logged&method=logged">Home</a></li>
          <li><a href="#about">Messages</a></li>
          <li><a href="index.php?controller=logged&method=search">Search</a></li>
          <li><a href="index.php?controller=logged&method=settings">Settings</a></li>
          <li><a href="index.php?controller=logged&method=disconnect">Disconnect</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<script type="text/javascript" src="View/script.js"></script>
<div id='user_id'>
  <?php
    if (isset($_SESSION['user_id'])) {
      echo $_SESSION['user_id'];
    }
    else {
      echo 'none';
    }
   ?>
</div>

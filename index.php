<?php

/*

*/
session_start();


// if (!isset($_SESSION['user_id'])) {
//   include 'View/login.php';
// }
// else {
  if(isset($_GET['controller']) &&
      file_exists('Controller/' . $_GET['controller'] . 'Controller.php')) {

    $controller = $_GET['controller'];
    require_once("Controller/${controller}Controller.php");
    $class = "${controller}Controller";
    $var = new $class();

    if (isset($_GET['method']) && method_exists($var, $_GET['method']) &&
     (key_exists('user_id', $_SESSION) || $_GET['method'] == 'login'
      || $_GET['method'] == 'signin')) {
      $method = $_GET['method'];
      $var->$method();
    }
    else {
      session_destroy();
      include 'View/login.php';
    }

  }
  else {
    session_destroy();
    include 'View/login.php';
  }
// }


// require_once("Controller/${controller}Controller.php");
// $class = "${controller}Controller";
// $var = new $class();
// $var->$method();

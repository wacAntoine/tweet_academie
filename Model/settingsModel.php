<?php
include 'formsModel.php';
class settingsModel {
  public function change_username() {
    include 'database.php';
    $db = database::connect();
    $pre = $db->prepare("
    UPDATE accounts
    SET username = :username
    WHERE accounts.user_id = :id;
    ");
    $pre->bindParam(":username" , $_POST['username']);
    $pre->bindValue(":id" , $_SESSION['user_id']);
    $pre->execute();
    $obj = $pre->fetchAll(PDO::FETCH_ASSOC);
    return $obj;
  }
  
  public function change_email() {
  	$email = $_POST['new_email'];
	  $user_id = $_SESSION['user_id'];

  	// include 'Model/formsModel.php';
	  $valid = 0;
  	$model = new formsModel;
  	if (!$model->check_email($email)) {
    	return 'bad';
	    $valid++;
    }
    if (!$model->check_email_exist($email)) {
    	return 'exists';
        $valid++;
    }


    $dbh = database::connect();
    $requette = "
    UPDATE accounts
    SET email = :email
    WHERE accounts.user_id = :user_id
    ;";
    $requettePrepare = $dbh->prepare($requette);
    $requettePrepare->bindParam(':email', $email, PDO::PARAM_STR);
    $requettePrepare->bindParam(':user_id', $user_id, PDO::PARAM_STR);


    if ($valid == 0){
   		$requettePrepare->execute();
    }
  }

//   public function change_password() {
//     include 'database.php';
//     $db = database::connect();
//     requette1 = "SELECT "
//   }
// }


<?php
class tweetController {
  public function send_tweet() {
    if (isset($_POST['tweet'])
      && isset($_POST['user_id'])
      && $_POST['user_id'] == $_SESSION['user_id']) {
        $user_id = $_POST['user_id'];
        $tweet = $_POST['tweet'];
    }
    else {
      echo 'error';
      return 0;
    }

    if (strlen($tweet) >= 141 || strlen($tweet) < 5) {
      echo 'more_than_140';
      return 0;
    }
    include 'Model/tweetModel.php';
    $model = new tweetModel;
    $model->send_tweet($tweet);
  }

  public function get_tweets_ajax() {
    include 'Model/tweetModel.php';
    $model = new tweetModel;
    $result = $model->get_tweets_model();
    echo json_encode($result);
  }

  public function send_retweet() {
    // include 'Model/tweetModel.php';
    include 'Model/tweetModel.php';
    $model = new tweetModel;
    $user_id = $_SESSION['user_id'];
    $tweet_id = $_POST['tweet_id'];

    return $model->retweet_model($tweet_id, $user_id);

  }
}


// $var = new  tweetController;
// var_dump($var->send_retweet());


// SELECT *
// FROM tweet
// WHERE user_id IN ()
// ORDER BY date
// LIMIT 15;

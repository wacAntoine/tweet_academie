<?php
class formsModel {
  public function check_username($str) {
    if (strlen($str) > 3 ) {
      return true;
    }
    return false;
  }

  public function check_password($str,$str2) {
  	if (strlen($str) >= 3 && $str === $str2) {
  		return true;
  	}
  	return false;
  }
  public function check_email($str) {
  	if (preg_match(' /^[\w\-\+]+(\.[\w\-]+)*@[\w\-]+(\.[\w\-]+)*\.[\w\-]{2,4}$/', $str)){
  		return true;
  	}
  	return false;
  }
  public function check_login_filds() {
    $valid = 0;
    if (array_key_exists('email', $_POST)) {
      $valid++;
    }
    if (array_key_exists('pass', $_POST)) {
      $valid++;
    }
    if ($valid == 2) {
      return true;
    }
    return false;
  }

  public function check_signin_filds() {
  	$valid = 0;

  	if (array_key_exists('username', $_POST)){
  		$valid++;
  	}
  	if (array_key_exists('email', $_POST)){
  		$valid++;
  	}
  	if (array_key_exists('pass', $_POST)){
  		$valid++;
  	}
  	if (array_key_exists('pass_verif', $_POST)){
  		$valid++;
  	}

    //what if more then 4
  	if ($valid == 4){
  		return true;
  	}
  	return false;
  }

  public function ckeck_singnin_filds_empty($str1, $str2, $str3, $str4){
  	if (!$str1 == '' || !$str2 == '' || !$str3 == '' || !$str4 == '' ){
  		return true;
  	}
  	return false;
  }

  public function check_email_exist($email){
    include_once 'database.php';
    $dbh = database::connect();
    $requette = "SELECT email
                FROM accounts
                Where email LIKE :email;";
  	$requettePrepare = $dbh->prepare($requette);
    $requettePrepare->bindParam(':email', $email, PDO::PARAM_STR);
  	$requettePrepare->execute();
  	$email_check = $requettePrepare->fetchAll(PDO::FETCH_ASSOC);
  	if (count($email_check) == 0){
      return true;
  	}
  	return false;
  }

  public function send_sign_in() {
    // include 'database.php'; already included before
    $dbh = database::connect();
    $name = $_POST['username'];
    $email = $_POST['email'];
    $pass = $_POST['pass'];

    $requette = "
    INSERT INTO accounts
    (username, email, password)
    VALUES (:name , :email, :pass_hash)
    ";
    $requettePrepare = $dbh->prepare($requette);
    $pass_salt = "$pass si tu aimes la wac tape dans tes mains";
    $pass_hash = hash('ripemd160', $pass_salt);

    $requettePrepare->bindParam(':name', $name, PDO::PARAM_STR);
    $requettePrepare->bindParam(':email', $email, PDO::PARAM_STR);
    $requettePrepare->bindParam(':pass_hash', $pass_hash, PDO::PARAM_STR);

    $requettePrepare->execute();
  }

  public function check_login($email, $pass) {
    include 'database.php';
    $db = database::connect();
    $prepared = $db->prepare('
    SELECT email, password
    FROM accounts
    WHERE email
    LIKE :email
    ;');
    $prepared->bindParam(':email', $email, PDO::PARAM_STR);
    $prepared->execute();

    $obj = $prepared->fetchAll(PDO::FETCH_ASSOC);
    $pass_salt = "$pass si tu aimes la wac tape dans tes mains";
    $pass_hash = hash('ripemd160', $pass_salt);

    if (isset($obj[0]['password']) && $pass_hash == $obj[0]['password']) {
      return true;
    }
    return false;
  }
}
// $var = new formsModel;
// var_dump($var->check_login('asdf@asdf.asdf', 'asdf'));

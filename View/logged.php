<!DOCTYPE html>
<html>
<head>
  <?php include 'head.php'; ?>
  <link rel="stylesheet" href="View/style1.css">
</head>
<body>
  <?php include 'header.php'; ?>

  <div class="profil">
    <div class="container">
       <div class="row">
        <div class="col-md-2 col-sm-12">
          <img src = "View/images/tux.png" class = "img-circle">
        </div>
        <div class="col-md-5 col-sm-8">
          <?php
          echo '<b>Username</b> : ' . $_SESSION['username'] . '<br>';
          echo '<b>Email</b> : ' . $_SESSION['email'] . '<br>';
           ?>
          Le Lorem Ipsum est simplement du faux texte employé survivre cinq siècles, mais s'est aussi adapté à la bureautique<br/> informatique, sans que son contenu n'en soit modifié.
        </div>
        <div class="col-md-5 col-sm-4">
          <div class="btn-group-vertical">
            <a href="index.php?controller=follow&method=followers_view"><button type="button" class="btn btn-default">Followers</button>
            </a>
            <a href="index.php?controller=follow&method=followed_view"><button type="button" class="btn btn-default">Followed</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">


    <div id='tweet_id' class="input-group">
      <textarea id='tweet_field' class="form-control custom-control" rows="3" style="resize:none"></textarea>
      <span id='tweet' class="input-group-addon btn btn-primary">Twitter</span>
    </div>

    <div id='tweets_div'>

    </div>


  </div>

  <footer class="col-sm-12">
    <center>Twitter Copyright &copy; - Tous droits réservés.</center>
  </footer>

</body>
</html>

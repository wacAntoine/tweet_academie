window.addEventListener("load", function(event) {
  $("#send_username_change").on("click", function(){
  	send_username_change();
  });

  $("#send_email_change").on("click", function(){
    send_email_change();
  });

  $('#tweet').on('click', function() {
    if ($('#user_id').html() == 'none') {
      alert('You have to log in in order to tweet');
      return 0;
    }
    send_tweet();
  });

  $('#searchButton').on('click', function() {
    send_search_username();
  });

  // $('#buttonNewPassword').on('click' function() {
  //   change_password();
  // })

  $('body').on('click', function(event) {
    var text = $(event.target).attr('id');
    if(text == undefined) {
      return 0;
    }
    if (text.search('retweet') == 0) {
      send_retweet(text.substring(7));
    }
    if (text.search('follow') == 0) {
      send_follow(text.substring(6));
    }
  });


  window.setInterval(function(){
    console.log('refersh');
    get_tweets();
  }, 20000);

  get_tweets();
});

function send_follow(text) {
  $.ajax({
    url: "index.php?controller=follow&method=send_follow_controller",
    type: 'POST',
    data: {'followed_id': text},
    success: function(code_html,statut) {
    }
  });
}

function send_retweet(text) {
  $.ajax({
    url: "index.php?controller=tweet&method=send_retweet",
    type: 'POST',
    data: {'tweet_id': text},
    success : function(code_htlm, statut){
      // alert("bonjour");
    },
  });
  $('#tweets_div').html('');
  $('#tweet_field').val('');
  get_tweets();
}

function send_tweet() {
  var user_id = $('#user_id').html();
  var tweet = $('#tweet_field').val();
  $.ajax({
  url: "index.php?controller=tweet&method=send_tweet",
  type: 'POST',
  data: {'tweet':tweet, 'user_id':user_id},
  success : function(code_html, statut){
      if (code_html == 'more_than_140') {
        alert("The tweet can't be longer than 140 characters and have to be 5 characters at least");
      }
      if (code_html == 'error') {
        alert('Something went wrong');
      }
    },
  });
    $('#tweets_div').html('');
    $('#tweet_field').val('');
    get_tweets();

}

function send_username_change() {
	var new_name = $('#name').val();
  $.ajax({
    url: "index.php?controller=settings&method=change_username",
    type: 'POST',
    data: {'username': new_name},
    success : function(code_htlm, statut){
    },
  });

}

function send_email_change() {
  var new_email = $('#input_email_change').val();
  var user_id = $('#user_id').html();
  $.ajax({
    url:"index.php?controller=settings&method=change_email",
    type:'POST',
    data: {'new_email':new_email, 'user_id':user_id},
    success: function(code_html, statut){
      if (code_html == 'exists') {
        alert('That email already exist');
      }
      if (code_html == 'bad') {
        alert('Invalid email');
      }
    }
  });
}

function send_search_username() {
  $.ajax({
    url: 'index.php?controller=search&method=search_username_controller',
    data: { username: $("#searchInput").val() },
    method: "POST",
    success: function(code_html, status) {
      $('#slotUser').html('');
      JSON.parse(code_html).forEach(function(x) {
        $("#slotUser").append(
          '<div class="well">' +
            "<b>Nom</b> : " + x.username +
            "<br><button id= 'follow" + x.user_id + "' class='btn btn-info'>follow</button>" +
          "</div>"
        );
      });
    }
  });
}

// function change_password() {
//   var lastPassword = $('lastPassword').val();
//   var newPassword = $('newPassword').val();
//   var newPasswordVerif = $('newPasswordVerif').val();
//   $.ajax({
//     url: 'index.php?controller=change&method=',
//     data: { lastPassword: lastPassword, newPassword: newPassword, newPasswordVerif: newPasswordVerif},
//     method: "POST",
//     success: function(code_html, statut) {
//     }
//   })
// }

function get_tweets() {
  $('#tweets_div').html('');
  $.ajax({
    url: 'index.php?controller=tweet&method=get_tweets_ajax',
    type:'POST',
    success: function(code_html, statut){
      JSON.parse(code_html).forEach(function(x) {
        $('#tweets_div').append(
          '<div class="well">' +
            '<b>username : </b>' + x.username +
            '<br>' +
            '<b>date : </b>' + x.tweeted_at +
            '<br>' +
            x.content +
            '<br><button class="btn btn-primary">response</button>' +
            '<button id="retweet' + x.tweet_id +
            '" class="btn btn-info">re-tweet</button>' +
          '</div>');
      });
    }
  });
}
